import React from 'react';
import Header from './Header.jsx';
import Footer from './Footer.jsx';

class App extends React.Component {
   render() {
      return (
        <div>
            <Header />
            <Footer />
        </div>
      );
   }
}

export default App;